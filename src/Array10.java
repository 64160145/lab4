import java.util.Scanner;
import java.util.function.UnaryOperator;

public class Array10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input number of element: ");
        int NumberElement = 0;
        int arr[];
        int uniqesize= 0;
        NumberElement = sc.nextInt();
        arr = new int[NumberElement];
        for(int i = 0;i < arr.length;i++){
            System.out.print("Element " + i +" : ");
            int temp =sc.nextInt();
            int index = -1;
            for(int j = 0;j<uniqesize;j++){
                if(arr[j] == temp){
                    index = j;
                }
            }
            if(index < 0){
                arr[uniqesize] = temp;
                uniqesize++;
            }
        }
        System.out.print("All number = ");
        for(int i = 0;i <uniqesize;i++){
            System.out.print(arr[i]+ " ");
        }
    }
}
