import java.util.Scanner;

import javax.print.CancelablePrintJob;

public class MyApp {
    public static void main(String[] args) {
        while(true){
            int choice = 0;
        printWelcome();
        printMenu();
        choice = inputChoice();
        process(choice);
    }
}
        
    private static void process(int choice) {
        switch(choice){
            case 1:
            printhelloworld();
            break;
            case 2:
            addNumber();
            break;
            case 3:
            exitProgram();
            break;  
        }
    }
    private static int add(int first,int second) {
        int result = first + second;
        return result;
    }
    private static void addNumber() {
        Scanner sc = new Scanner(System.in);
        int first;
        int second;
        int result;
        System.out.print("Please input first number: ");
        first = sc.nextInt();
        System.out.print("Please input second number: ");
        second = sc.nextInt();
        result = add(first,second);
        System.out.println("Result = " + result);
    }
    private static void printhelloworld() {
        Scanner sc = new Scanner(System.in);
        int time;
        System.out.print("Please input time: ");
        time = sc.nextInt();
        for(int i = 0;i < time;i++){
            System.out.println("Hello World!!!");
        }
    }
    private static void exitProgram() { 
        System.out.println("Bye!!!");
        System.exit(0);
    }

    private static int inputChoice() {
        int choice;
        Scanner sc = new Scanner(System.in);
        while(true){
        System.out.print("Please input your choice(1-3): ");
        choice = sc.nextInt();
        if(choice>=1 && choice<= 3){
            return choice;
        }
        System.out.println("Error: Please input between 1-3");
        }  
    }

    private static void printMenu() {
        System.out.println("--Menu--");
        System.out.println("1. Print Hello world N time");
        System.out.println("2. Add 2 number");
        System.out.println("3. Exit");
    }

    private static void printWelcome() {
        System.out.println("Welcome to my app!!!");
    }
}